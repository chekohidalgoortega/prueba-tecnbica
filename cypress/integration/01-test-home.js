context("Check Home And timeline", () => {
  it("Visit homePage and wait until everythimg is loaded", () => {
    cy.visit(Cypress.env("url"));
    cy.get("div[data-cypress=followsBox]", { timeout: 10000 });
  });

  it("Change language to 'En'", () => {
    cy.get("div[data-cypress=langSelector]").click();
    cy.get("div[role=presentation]").last().find("li").contains("En").click();
    cy.get("div[data-cypress=followsBox]")
      .find("p")
      .should("have.text", "Following:");
  });

  it("Change language to 'Es'", () => {
    cy.get("div[data-cypress=langSelector]").click();
    cy.get("div[role=presentation]").last().find("li").contains("Es").click();
    cy.get("div[data-cypress=followsBox]")
      .find("p")
      .should("have.text", "Siguiendo a:");
  });

  it("Unfollow user", () => {
    cy.get(
      "div[data-cypress=followsBox] div[data-cypress=4a470e0e-6cdf-481e-922f-daf4613e2f27]"
    )
      .find("button")
      .click();
    cy.wait(2000);
    cy.get(
      "div[data-cypress=followsBox] div[data-cypress=4a470e0e-6cdf-481e-922f-daf4613e2f27]"
    ).should("not.exist");
    cy.get("div[data-cypress=timelineBox]")
      .find("div[data-cypress-owner=4a470e0e-6cdf-481e-922f-daf4613e2f27]")
      .should("not.exist");
  });

  it("Follow user", () => {
    cy.get(
      "div[data-cypress=followersBox] div[data-cypress=f5e44c07-7cbb-4157-969b-99c77a284f88]"
    )
      .find("button")
      .click();
    cy.wait(2000);
    cy.get(
      "div[data-cypress=followsBox] div[data-cypress=f5e44c07-7cbb-4157-969b-99c77a284f88]"
    ).should("exist");
    cy.get("div[data-cypress=timelineBox]")
      .find("div[data-cypress-owner=f5e44c07-7cbb-4157-969b-99c77a284f88]")
      .should("exist");
  });

  it("Filter timeline by user", () => {
    cy.get("div[data-cypress=usersFilter]").click();
    cy.get("div[data-cypress=usersFilter]")
      .find("input[type=text]")
      .type("wat", { delay: 250 })
      .should("have.value", "wat");
    cy.get("div[data-cypress=usersFilter]")
      .parent()
      .find("div[role=presentation] li")
      .first()
      .click();
    cy.get("div[data-cypress=timelineBox]")
      .find("div[data-cypress-owner=627a19cc-9130-4945-9980-985bbd58c153]")
      .should("exist");
    cy.get("div[data-cypress=usersFilter] button").first().click();
  });

  it("Post tweet", () => {
    cy.fixture("example.json").then((fixtures) => {
      cy.get("div[data-cypress=newPostTextarea] textarea")
        .first()
        .type(fixtures.tweet, { delay: 50 })
        .should("have.value", fixtures.tweet);
      cy.get("div[data-cypress=newPostTextarea] p").should(
        "have.text",
        "Has escrito 150 caracteres de los 250 posibles"
      );

      cy.get("button[data-cypress=postBtn]").click();
      cy.wait(1000);
      cy.get("div[data-cypress=newPostTextarea] textarea")
        .first()
        .should("have.value", "");
      cy.get("div[data-cypress=tweet]")
        .first()
        .find("p[data-cypress=tweetContent]")
        .should("have.text", fixtures.tweet);
    });
  });
});
