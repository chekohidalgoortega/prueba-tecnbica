import { Grid } from "@mui/material";
import React, { useEffect, useState } from "react";
import styled from "styled-components";
import getUserTimeline from "../../Api/Tweets/getUserTimeline";
import getUserFollowers from "../../Api/User/getUserFollowers";
import getUserFollows from "../../Api/User/getUserFollows";
import Layout from "../../Components/Shared/Layout/Layout";
import NewMessageArea from "../../Components/Tweets/NewMessageArea/NewMessageArea";
import Timeline from "../../Components/Tweets/Timeline/Timeline";
import FollowBox from "../../Components/User/FollowBox";
import FollowersBox from "../../Components/User/FollowersBox";
import { useAppContext } from "../../Context/AppContext";
import { Tweet } from "../../Types/App";
import { User } from "../../Types/User";

const HomeWrapper = styled.div`
  width: 100%;
  padding: 0 24px;
  margin: 20px auto;
`;

const MainContentWrapper = styled.div`
  width: 100%;
  padding: 10px;
  min-height: 40px;
`;

const Home: React.FC = () => {
  const { user, setBackDrop } = useAppContext();
  const [followers, setFollowers] = useState<User[]>();
  const [usersFollows, setUsersFollows] = useState<User[]>();
  const [timeline, setTimeline] = useState<Tweet[]>();

  useEffect(() => {
    const doAsyncTask = async () => {
      if (user) {
        setBackDrop(true);
        const fetchFollows = await getUserFollows(user.uuid);
        const fetchFollowers = await getUserFollowers(user.uuid);
        const fetchTimeline = await getUserTimeline(user, fetchFollows ?? []);
        setUsersFollows(fetchFollows);
        setFollowers(fetchFollowers);
        setTimeline(fetchTimeline);
        setBackDrop(false);
      }
    };

    doAsyncTask();
  }, []);

  useEffect(() => {
    const doAsync = async () => {
      const fetchTimeline = await getUserTimeline(user, usersFollows ?? []);
      const workingTimeline = timeline ?? [];
      let updatedTimeline = workingTimeline.concat(fetchTimeline);
      updatedTimeline = updatedTimeline.filter((item, index) => {
        return (
          updatedTimeline.indexOf(item) == index &&
          (usersFollows ?? []).find((el) => el.uuid === item.owner)
        );
      });
      updatedTimeline = updatedTimeline.sort((a, b) =>
        a.date < b.date ? 1 : b.date < a.date ? -1 : 0
      );
      setTimeline(updatedTimeline);
    };

    doAsync();
  }, [usersFollows]);

  return (
    <Layout>
      <HomeWrapper>
        <Grid container sx={{ width: "100%" }}>
          <Grid
            item
            xs={12}
            lg={3}
            md={4}
            sx={{ minHeight: "80vh", padding: "0 10px 0 0" }}
          >
            {user && usersFollows && (
              <FollowBox
                followers={followers ?? []}
                setFollowers={setFollowers}
                follows={usersFollows}
                setFollows={setUsersFollows}
              />
            )}
            {user && followers && (
              <FollowersBox
                followers={followers}
                setFollowers={setFollowers}
                setFollows={setUsersFollows}
                follows={usersFollows ?? []}
              />
            )}
          </Grid>
          <Grid
            item
            xs={12}
            lg={9}
            md={8}
            sx={{ minHeight: "80vh", padding: "0 0 0 10px" }}
          >
            <MainContentWrapper>
              {user && (
                <NewMessageArea
                  timeline={timeline ?? []}
                  setTimeline={setTimeline}
                />
              )}
              {timeline && usersFollows && (
                <Timeline timeline={timeline} usersFollows={usersFollows} />
              )}
            </MainContentWrapper>
          </Grid>
        </Grid>
      </HomeWrapper>
    </Layout>
  );
};

export default Home;
