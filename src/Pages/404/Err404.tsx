import { Container, Grid, Typography } from "@mui/material";
import React from "react";
import Layout from "../../Components/Shared/Layout/Layout";
import Error from "@mui/icons-material/Report";
import { useTranslation } from "react-i18next";

const Err404: React.FC = () => {
  const { t } = useTranslation("err404");

  return (
    <Layout>
      <Container>
        <Grid
          sx={{ height: "100%", paddingTop: "100px" }}
          container
          direction="column"
          alignItems="center"
          alignContent="center"
          justifyContent="center"
        >
          <Grid
            container
            spacing={2}
            direction="column"
            alignItems="center"
            justifyContent="center"
          >
            <Grid item>
              <Error color="error" sx={{ fontSize: "20rem" }} />
            </Grid>
            <Grid item>
              <Typography variant="h5" component="h3">
                {t("err404Msg")}
              </Typography>
            </Grid>
          </Grid>
        </Grid>
      </Container>
    </Layout>
  );
};

export default Err404;
