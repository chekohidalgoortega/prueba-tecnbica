import React, { useState, useContext } from "react";
import i18n from "../Translations/i18n";
import { Langs } from "../Types/App";
import { CurrentUser } from "../Types/User";

export interface AppContextInterface {
  userAuthenticated: boolean;
  setUserAuthenticated: (isAuthenticated: boolean) => void;
  setUser: (user: CurrentUser | null) => void;
  user: CurrentUser | null;
  lang: Langs;
  changeLang: (val: Langs) => void;
  backDrop: boolean;
  setBackDrop: (val: boolean) => void;
}

const AppContext = React.createContext<AppContextInterface>({
  userAuthenticated: false,
  setUserAuthenticated: () => undefined,
  user: { name: "", uuid: "", thumbnail: "", token: "" },
  setUser: () => undefined,
  lang: "es",
  changeLang: () => undefined,
  backDrop: false,
  setBackDrop: () => undefined
});

export default AppContext;

export const AppContextProvider = AppContext.Provider;

export const useContextInApp = (): AppContextInterface => {
  const [userAuthenticated, setUserAuthenticated] = useState(true);
  const [user, setUser] = useState<CurrentUser | null>({
    name: "David Gilmour",
    uuid: "56c01e8a-26ce-4cb5-942e-6068abf66617",
    thumbnail: "https://64.media.tumblr.com/avatar_9e113bd2b886_128.pnj",
    token: "f362de2501ab468889314b8958e81b6c"
  });
  const storedLang = localStorage.getItem("i18nextLng");
  const [lang, setLang] = useState<Langs>(
    (storedLang?.split("-")[0] as Langs) ?? ("es" as Langs)
  );
  const [backDrop, setBackDrop] = useState(false);

  const changeLang = (lang: Langs) => {
    i18n.changeLanguage(lang);
    setLang(lang);
  };

  const contextValues: AppContextInterface = {
    userAuthenticated,
    setUserAuthenticated,
    setUser,
    user,
    lang,
    changeLang,
    backDrop,
    setBackDrop
  };

  return contextValues;
};

export const useAppContext = (): AppContextInterface => useContext(AppContext);
