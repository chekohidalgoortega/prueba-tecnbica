import React, { lazy } from "react";
import { BrowserRouter, Route, Routes } from "react-router-dom";

const HomePage = lazy(() => import("../Pages/Home/Home"));
const Err404Page = lazy(() => import("../Pages/404/Err404"));

const AppRoutes: React.FC = () => {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<HomePage />} />
        <Route path="*" element={<Err404Page />} />
      </Routes>
    </BrowserRouter>
  );
};

export default AppRoutes;
