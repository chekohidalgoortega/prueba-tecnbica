import React, { Suspense } from "react";
import { AppContextProvider, useContextInApp } from "./Context/AppContext";
import CssBaseline from "@mui/material/CssBaseline";
import AppRoutes from "./Config/AppRoutes";
import { CircularProgress } from "@mui/material";

const App: React.FC = () => {
  const contextValues = useContextInApp();

  return (
    <Suspense fallback={<CircularProgress />}>
      <AppContextProvider value={contextValues}>
        <CssBaseline />
        <AppRoutes />
      </AppContextProvider>
    </Suspense>
  );
};

export default App;
