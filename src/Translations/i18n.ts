import i18n from "i18next";
import { initReactI18next } from "react-i18next";

import LanguageDetector from "i18next-browser-languagedetector";
import es from "./Languages/es.json";
import en from "./Languages/en.json";

i18n
  .use(LanguageDetector)
  .use(initReactI18next)
  .init({
    fallbackLng: "es",
    debug: false,
    saveMissing: false,
    resources: {
      es,
      en
    },
    interpolation: {
      escapeValue: false
    }
  });

export default i18n;
