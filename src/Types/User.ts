export interface User {
  uuid: string;
  name: string;
  thumbnail: string;
  follow?: boolean;
}

export interface CurrentUser extends User {
  token: string;
}
