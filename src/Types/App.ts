export type Langs = "es" | "en";

export type Tweet = {
  owner: string;
  name: string;
  date: number;
  text: string;
  thumbnail: string;
};
