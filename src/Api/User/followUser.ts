// eslint-disable-next-line @typescript-eslint/no-unused-vars
export default async (userUuid: string, userTofollow: string) => {
  const response = await new Promise((resolve) => {
    setTimeout(() => {
      resolve("ok");
    }, 1000);
  });
  return response;
};
