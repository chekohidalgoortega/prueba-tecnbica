import follows from "../../Fixtures/follow.json";
import { User } from "../../Types/User";

// eslint-disable-next-line @typescript-eslint/no-unused-vars
export default async (userUuid: string) => {
  const response = await new Promise((resolve) => {
    setTimeout(() => {
      resolve(follows);
    }, 1000);
  });
  return response as User[];
};
