import followers from "../../Fixtures/followers.json";
import { User } from "../../Types/User";

// eslint-disable-next-line @typescript-eslint/no-unused-vars
export default async (userUuid: string) => {
  const response = await new Promise((resolve) => {
    setTimeout(() => {
      resolve(followers);
    }, 1500);
  });
  return response as User[];
};
