import { User } from "../../Types/User";
import Tweets from "../../Fixtures/tweets.json";
import { Tweet } from "../../Types/App";

export default async (currentUser: User | null, follows: User[]) => {
  const response = await new Promise((resolve) => {
    setTimeout(() => {
      const owners = follows.map((el) => el.uuid);
      if (currentUser) {
        owners.push(currentUser.uuid);
      }
      let res = Tweets.filter((el) => owners.includes(el.owner));
      res = res.sort((a, b) =>
        a.date < b.date ? 1 : b.date < a.date ? -1 : 0
      );
      resolve(res);
    }, 1000);
  });
  return response as Tweet[];
};
