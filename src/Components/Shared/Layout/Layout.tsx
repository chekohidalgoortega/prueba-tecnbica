import React from "react";
import { useAppContext } from "../../../Context/AppContext";
import TopBar from "./TopBar";
import "./Layout.css";
import { Backdrop, CircularProgress } from "@mui/material";

const Layout: React.FC = (props) => {
  const { user, backDrop } = useAppContext();
  return (
    <>
      <TopBar user={user} />
      <Backdrop sx={{ color: "#fff", zIndex: 99 }} open={backDrop}>
        <CircularProgress color="inherit" />
      </Backdrop>
      <main className="mainContent">{props.children}</main>
    </>
  );
};

export default Layout;
