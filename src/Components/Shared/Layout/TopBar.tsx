import * as React from "react";
import AppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import Toolbar from "@mui/material/Toolbar";
import IconButton from "@mui/material/IconButton";
import Typography from "@mui/material/Typography";
import Menu from "@mui/material/Menu";
import Avatar from "@mui/material/Avatar";
import Tooltip from "@mui/material/Tooltip";
import MenuItem from "@mui/material/MenuItem";
import { CurrentUser } from "../../../Types/User";
import styled from "styled-components";
import { useAppContext } from "../../../Context/AppContext";
import { Button } from "@mui/material";
import { Link } from "react-router-dom";
import LangSelector from "./LangSelector";

const settings = ["Profile", "Account", "Dashboard", "Logout"];

const Logo = styled.img`
  width: 32px;
  height: 32px;
`;

const TopBarUserBox = styled.div`
  width: 100%;
  text-align: right;
`;

interface TopBarProps {
  user: CurrentUser | null;
}

const TopBar: React.FC<TopBarProps> = ({ user }) => {
  const { userAuthenticated } = useAppContext();

  const [anchorElUser, setAnchorElUser] = React.useState<null | HTMLElement>(
    null
  );

  const handleOpenUserMenu = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorElUser(event.currentTarget);
  };

  const handleCloseUserMenu = () => {
    setAnchorElUser(null);
  };

  return (
    <Box sx={{ flexGrow: 1 }}>
      <AppBar position="static">
        <Toolbar>
          <Link to="/">
            <IconButton
              size="large"
              edge="start"
              color="inherit"
              aria-label="menu"
              sx={{ mr: 2 }}
            >
              <Logo src="/logo192.png" alt="logo" />
            </IconButton>
          </Link>
          <TopBarUserBox>
            <LangSelector />
            {userAuthenticated && user && (
              <>
                <Tooltip title="Open settings">
                  <IconButton onClick={handleOpenUserMenu} sx={{ p: 0 }}>
                    <Avatar alt="Remy Sharp" src={user.thumbnail} />
                  </IconButton>
                </Tooltip>
                <Menu
                  sx={{ mt: "45px" }}
                  id="menu-appbar"
                  anchorEl={anchorElUser}
                  anchorOrigin={{
                    vertical: "top",
                    horizontal: "right"
                  }}
                  keepMounted
                  transformOrigin={{
                    vertical: "top",
                    horizontal: "right"
                  }}
                  open={Boolean(anchorElUser)}
                  onClose={handleCloseUserMenu}
                >
                  {settings.map((setting) => (
                    <MenuItem key={setting}>
                      <Typography textAlign="center">{setting}</Typography>
                    </MenuItem>
                  ))}
                </Menu>
              </>
            )}
            {!userAuthenticated && (
              <Tooltip title="Open settings">
                <Button color="inherit">Login</Button>
              </Tooltip>
            )}
          </TopBarUserBox>
        </Toolbar>
      </AppBar>
    </Box>
  );
};
export default TopBar;
