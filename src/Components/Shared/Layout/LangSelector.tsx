import { MenuItem, Select, SelectChangeEvent } from "@mui/material";
import React from "react";
import { useAppContext } from "../../../Context/AppContext";
import { Langs } from "../../../Types/App";

const LangSelector: React.FC = () => {
  const { lang, changeLang } = useAppContext();
  console.log(lang);

  const handleChange = (event: SelectChangeEvent) => {
    changeLang(event.target.value as Langs);
  };

  return (
    <Select
      value={lang}
      id="langs"
      sx={{ border: 0, marginRight: "8px" }}
      label="Age"
      onChange={handleChange}
      disableUnderline
      data-cypress="langSelector"
    >
      <MenuItem value="es">Es</MenuItem>
      <MenuItem value="en">En</MenuItem>
    </Select>
  );
};

export default LangSelector;
