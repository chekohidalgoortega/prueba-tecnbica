import { Button, Paper, Typography } from "@mui/material";
import React, { useState } from "react";
import { useTranslation } from "react-i18next";
import styled from "styled-components";
import followUser from "../../Api/User/followUser";
import { useAppContext } from "../../Context/AppContext";
import { User } from "../../Types/User";
import UserListItem from "./Common/UserListItem";

const UsersBox = styled.div`
  padding: 5px;
`;

const SeeMore = styled.div`
  text-align: right;
  display: block;
`;

interface FollowersBoxProps {
  followers: User[];
  follows: User[];
  setFollows: (val: User[]) => void;
  setFollowers: (val: User[]) => void;
}

const FollowersBox: React.FC<FollowersBoxProps> = ({
  followers,
  follows,
  setFollows,
  setFollowers
}) => {
  const { t } = useTranslation("FollowBox");
  const { user } = useAppContext();
  const [boxKey, setBoxKey] = useState(Date.now());
  const [seeAll, setSeeAll] = useState(false);

  const follow = async (userTofollow: string) => {
    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    const follow = await followUser(user!.uuid, userTofollow);
    if (follow) {
      const userIndex = followers.findIndex(
        (user) => user.uuid === userTofollow
      );

      if (typeof userIndex === "number") {
        const user = followers[userIndex];
        follows.push(user);
        setFollows([...follows]);

        user.follow = true;
        followers[userIndex] = user;
        setFollowers([...followers]);
        setBoxKey(Date.now());
      }
    }
  };

  return (
    <Paper
      data-cypress="followersBox"
      sx={{
        width: "100%",
        padding: "10px",
        marginBottom: "15px",
        minHeight: "40px"
      }}
      elevation={3}
    >
      <Typography variant="body1" sx={{ marginBottom: "8px" }}>
        {followers.length ? t("followersTitle") : t("followersEmptyTitle")}
      </Typography>
      {followers.length > 0 && (
        <UsersBox key={boxKey}>
          {}
          {followers.map((user, index) => {
            if (seeAll || (index < 4 && !seeAll)) {
              return (
                <UserListItem
                  key={user.uuid}
                  user={user}
                  action={follow}
                  btnType={"follow"}
                  hideActionBtn={user.follow}
                />
              );
            }
          })}
          {followers.length > 4 && !seeAll && (
            <SeeMore key={Date.now()}>
              <Button onClick={() => setSeeAll(true)}>
                {t("global:seeAll")} (+{followers.length - 4})
              </Button>
            </SeeMore>
          )}
        </UsersBox>
      )}
    </Paper>
  );
};

export default FollowersBox;
