import { Avatar, Button, CardHeader, CircularProgress } from "@mui/material";
import React, { useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import styled from "styled-components";
import { useAppContext } from "../../../Context/AppContext";
import { User } from "../../../Types/User";

const ListItem = styled.div`
  width: 100%;
  padding: 12px;
  position: relative;
  &:hover {
    background-color: #eee;
  }
`;

const ButtonWrapper = styled.div`
  position: absolute;
  right: 0;
  top: 16px;
`;

interface UserListItemProps {
  user: User;
  action: (val: string) => void;
  btnType: "follow" | "unfollow";
  hideActionBtn?: boolean;
}

const btnOverdrives = {
  backgroundColor: "#1976d2",
  color: "#fff",
  fontSize: "10px",
  fontWeight: "600",
  minWidth: "60px",
  marginRight: "8px",
  "&:hover": { backgroundColor: "#212121" }
};

const UserListItem: React.FC<UserListItemProps> = ({
  user,
  action,
  btnType,
  hideActionBtn
}) => {
  const { lang } = useAppContext();
  const { t } = useTranslation("FollowBox");
  const [txt, setTxt] = useState(
    btnType === "unfollow" ? t("btnTextUnfollow") : t("btnTextFollow")
  );
  const [btnDisabled, setBtnDisabled] = useState(false);

  useEffect(() => {
    if (!btnDisabled) {
      setTxt(
        btnType === "unfollow" ? t("btnTextUnfollow") : t("btnTextFollow")
      );
    } else {
      setTxt(
        btnType === "unfollow" ? t("btnTextUnfollowing") : t("btnTextFollowing")
      );
    }
  }, [lang]);

  const doBtnAction = () => {
    setBtnDisabled(true);
    setTxt(t("btnTextUnfollowing"));
    action(user.uuid);
  };

  return (
    <ListItem data-cypress={user.uuid}>
      <CardHeader
        sx={{ padding: 0 }}
        title={user.name}
        avatar={<Avatar alt={user.name} src={user.thumbnail} />}
      />
      <ButtonWrapper>
        {hideActionBtn !== true && (
          <Button
            disabled={btnDisabled}
            sx={btnOverdrives}
            onClick={doBtnAction}
          >
            {btnDisabled ? <CircularProgress size={20} color="inherit" /> : txt}
          </Button>
        )}
      </ButtonWrapper>
    </ListItem>
  );
};

export default UserListItem;
