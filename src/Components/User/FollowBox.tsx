import { Button, Paper, Typography } from "@mui/material";
import React, { useState } from "react";
import { useTranslation } from "react-i18next";
import styled from "styled-components";
import unfollowUser from "../../Api/User/unfollowUser";
import { useAppContext } from "../../Context/AppContext";
import { User } from "../../Types/User";
import UserListItem from "./Common/UserListItem";

const UsersBox = styled.div`
  padding: 5px;
`;

const SeeMore = styled.div`
  text-align: right;
  display: block;
`;

interface FollowBoxProps {
  follows: User[];
  setFollows: (val: User[]) => void;
  followers: User[];
  setFollowers: (val: User[]) => void;
}

const FollowBox: React.FC<FollowBoxProps> = ({
  follows,
  setFollows,
  followers,
  setFollowers
}) => {
  const { t } = useTranslation("FollowBox");
  const { user } = useAppContext();
  const [boxKey, setBoxKey] = useState(Date.now());
  const [seeAll, setSeeAll] = useState(false);

  const unfollow = async (userToUnfollow: string) => {
    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    const unfollow = await unfollowUser(user!.uuid, userToUnfollow);
    if (unfollow) {
      const newFollows = follows.filter((user) => {
        return user.uuid !== userToUnfollow;
      });
      setFollows([...newFollows]);
      setBoxKey(Date.now());
    }

    const followerIndex = followers.findIndex(
      (el) => el.uuid === userToUnfollow
    );
    if (followerIndex >= 0) {
      followers[followerIndex].follow = false;
      setFollowers([...followers]);
    }
  };

  return (
    <Paper
      data-cypress="followsBox"
      elevation={3}
      sx={{
        marginTop: "10px",
        width: "100%",
        padding: "10px",
        marginBottom: "15px",
        minHeight: "40px"
      }}
    >
      <Typography variant="body1" sx={{ marginBottom: "8px" }}>
        {follows.length ? t("titleFollow") : t("titleFollowEmpty")}
      </Typography>
      {follows.length > 0 && (
        <UsersBox key={boxKey}>
          {follows.map((user, index) => {
            if (seeAll || (index < 4 && !seeAll)) {
              return (
                <UserListItem
                  key={index}
                  user={user}
                  action={unfollow}
                  btnType={"unfollow"}
                />
              );
            }
          })}
          {follows.length > 4 && !seeAll && (
            <SeeMore>
              <Button onClick={() => setSeeAll(true)}>
                {t("global:seeAll")} (+{follows.length - 4})
              </Button>
            </SeeMore>
          )}
        </UsersBox>
      )}
    </Paper>
  );
};

export default FollowBox;
