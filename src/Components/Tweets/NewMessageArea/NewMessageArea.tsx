import {
  Avatar,
  Button,
  Box,
  CircularProgress,
  Paper,
  TextField,
  Typography
} from "@mui/material";
import React, { useState } from "react";
import { useTranslation } from "react-i18next";
import styled from "styled-components";
import { useAppContext } from "../../../Context/AppContext";
import { Tweet } from "../../../Types/App";

const PostButtonWrapper = styled.div`
  text-align: right;
  padding-top: 8px;
`;

interface NewMessageAreaProps {
  timeline: Tweet[];
  setTimeline: (val: Tweet[]) => void;
}

const NewMessageArea: React.FC<NewMessageAreaProps> = ({
  timeline,
  setTimeline
}) => {
  const { user } = useAppContext();
  const { t } = useTranslation("postMessage");
  const [message, setMessage] = useState("");
  const [buttonDisabled, setButtonDisabled] = useState(false);

  const handleMsgChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setMessage(event.target.value);
  };

  const postMessage = () => {
    if (!user || message.length < 1) {
      setButtonDisabled(false);
      return;
    }
    setButtonDisabled(true);

    const newTweet: Tweet = {
      owner: user.uuid,
      name: user.name,
      date: Date.now(),
      text: message,
      thumbnail: user.thumbnail
    };

    const newTimeline = [newTweet, ...timeline];
    setTimeline(newTimeline);

    console.log(newTweet);
    setMessage("");
    setButtonDisabled(false);
  };

  return (
    <Paper elevation={2} sx={{ padding: "16px" }}>
      <Typography variant="body1" sx={{ marginBottom: "8px" }}>
        {t("title")}
      </Typography>
      <Box sx={{ display: "flex", flexDirection: "row" }}>
        <Avatar
          alt={user?.name ?? ""}
          title={user?.name ?? ""}
          src={user?.thumbnail}
          sx={{ marginRight: "16px", width: 48, height: 48, marginTop: "4px" }}
        />

        <TextField
          data-cypress="newPostTextarea"
          sx={{ width: "100%" }}
          multiline
          maxRows={4}
          value={message}
          onChange={handleMsgChange}
          inputProps={{ maxLength: 250 }}
          helperText={t("postHelper", { num: message.length })}
        />
      </Box>

      <PostButtonWrapper>
        <Button
          data-cypress="postBtn"
          sx={{ minWidth: "100px" }}
          variant="contained"
          disabled={buttonDisabled}
          onClick={postMessage}
        >
          {buttonDisabled ? (
            <CircularProgress size={25} color="inherit" />
          ) : (
            t("post")
          )}
        </Button>
      </PostButtonWrapper>
    </Paper>
  );
};

export default NewMessageArea;
