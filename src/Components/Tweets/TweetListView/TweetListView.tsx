import { Avatar, Paper, Typography } from "@mui/material";
import { Box } from "@mui/system";
import React from "react";
import styled from "styled-components";
import { useAppContext } from "../../../Context/AppContext";
import { Tweet } from "../../../Types/App";
import { getPostDate } from "./Utils/DateUtils";

const Date = styled.div`
  position: absolute;
  top: 8px;
  right: 0;
`;

interface TweetListViewProps {
  tweet: Tweet;
}

const TweetListView: React.FC<TweetListViewProps> = ({ tweet }) => {
  const { lang } = useAppContext();

  return (
    <Paper
      data-cypress="tweet"
      data-cypress-owner={tweet.owner}
      elevation={2}
      sx={{ marginBottom: "8px", padding: "12px" }}
    >
      <Box
        sx={{
          display: "flex",
          flexDirection: "row",
          marginBottom: "8px",
          position: "relative"
        }}
      >
        <Avatar
          alt={tweet.name}
          title={tweet.name}
          src={tweet.thumbnail}
          sx={{
            marginRight: "16px",
            width: 32,
            height: 32,
            marginTop: "4px",
            fontSize: "12px"
          }}
        />
        <Typography sx={{ marginTop: "8px", fontWeight: 700 }}>
          {tweet.name}
        </Typography>
        <Date>{getPostDate(tweet.date, lang)}</Date>
      </Box>
      <Box sx={{ paddingLeft: "48px" }}>
        <p data-cypress="tweetContent">{tweet.text}</p>
      </Box>
    </Paper>
  );
};

export default TweetListView;
