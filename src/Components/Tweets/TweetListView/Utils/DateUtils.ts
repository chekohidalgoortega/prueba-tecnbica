import { Langs } from "../../../../Types/App";
import i18next from "i18next";

export const getPostDate = (date: number, locale: Langs) => {
  let formatedDate = "";
  const diff = Math.ceil((Date.now() - date) / 1000);

  if (diff < 60) {
    formatedDate = i18next.t("time:now");
  } else if (diff < 3600) {
    formatedDate = i18next.t("time:xMin", { min: Math.ceil(diff / 60) });
  } else if (diff < 86400) {
    formatedDate = i18next.t("time:xHours", {
      hours: Math.ceil(diff / 60 / 60 - 1)
    });
  } else if (diff < 172800) {
    formatedDate = i18next.t("time:yesterday");
  } else {
    formatedDate = new Date(date).toLocaleDateString(locale);
  }

  return formatedDate;
};
