import { Paper, TextField } from "@mui/material";
import React, { useState } from "react";
import { Tweet } from "../../../Types/App";
import { User } from "../../../Types/User";
import TweetListView from "../TweetListView/TweetListView";
import Autocomplete from "@mui/material/Autocomplete";
import styled from "styled-components";
import { useAppContext } from "../../../Context/AppContext";
import { useTranslation } from "react-i18next";

const AutocompleteWrapper = styled.div`
  margin-bottom: 16px;
  text-align: right;
  display: flex;
  flex-direction: row-reverse;
`;

interface usersFollows {
  timeline: Tweet[];
  usersFollows: User[];
}

const Timeline: React.FC<usersFollows> = ({ timeline, usersFollows }) => {
  const { t } = useTranslation("timeline");
  const { user } = useAppContext();
  let filterOptions: User[] = [];
  if (user && usersFollows.findIndex((el) => el.uuid === user.uuid) === -1) {
    filterOptions = [user, ...usersFollows];
  }
  const [filterUser, setFilterUser] = useState<User | null>();

  return (
    <Paper
      data-cypress="timelineBox"
      elevation={2}
      sx={{ padding: "16px", marginTop: "32px" }}
    >
      <AutocompleteWrapper>
        <Autocomplete
          data-cypress="usersFilter"
          disablePortal
          options={filterOptions}
          getOptionLabel={(option) => option.name}
          sx={{ width: 300 }}
          renderInput={(params) => (
            <TextField {...params} label={t("filterLabel")} />
          )}
          onChange={(_event, value: User | null) => {
            setFilterUser(value ?? null);
          }}
        />
      </AutocompleteWrapper>
      <div>
        {timeline.map((el, index) => {
          if (!filterUser) {
            return <TweetListView key={index} tweet={el} />;
          } else {
            if (el.owner === filterUser.uuid) {
              return <TweetListView key={index} tweet={el} />;
            }
          }
        })}
      </div>
    </Paper>
  );
};

export default Timeline;
